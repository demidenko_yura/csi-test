package test.csi.service;

import org.springframework.stereotype.Service;
import test.csi.model.Price;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Service
public class PriceService {

    /**
     * Метод объединения текущих и новых цен для товаров.
     * <p>
     * Сначала мы добавляем все текущие цены в новую коллекцию, в которую будут добавляться новые цены.
     * Проходимся в цикле по новым ценам и сначала проверяем "уникальность" цены, если код продукта,
     * номер или отдел у новой цены не найден в текущих ценах, то добавляем новую цену в коллекциюю комбинированных цен.
     * <p>
     * Если среди текущих цен нашлась цена с таким же кодом продукта, номером и отделом, то перебираем текущие цены и сравниваем даты с новой ценой.
     * Если у новой цены дата начала меньше, чем дата окончания действия у текущей цены,
     * то далее сравниваем значение новой цены. Если значение новой цены равно значению текущей цены,
     * то мы увеличиваем срок действия текущей цены. Новую цену не добавляем.
     * <p>
     * Если значения у цен разные, сравниваем даты, если пересекаюся и:
     * - если новая цена была добавлена ранее, то сравниваем дату начала текущей цены и дату окончания новой,
     * если текущая цена начинается до того, как закачивается новая, то меняем дату начала текущей цены.
     * <p>
     * - если новая цена была добавлена ранее, и даты новой цены и текущей не пересекаются, то продолжаем перебор новых цен, ничего не меняем.
     * <p>
     * - если новая цена еще не была добавлена в коллекцию и дата окончания новой цены меньше даты окончания текущей,
     * то от текущей цены берем остаток, уменьшаем действие текущей цены, сохраняем новую, сохраняем остаток как отдельную цену.
     *
     * @param currentPrices коллекция текущих цен
     * @param newPrices     коллекция новых цен
     * @return коллекция объединенных цен
     */
    public List<Price> combinePrices(List<Price> currentPrices, List<Price> newPrices) {
        List<Price> combinedPrices = new ArrayList<>(currentPrices);

        for (Price newPrice : newPrices) {
            if (combinedPrices.stream().noneMatch(price -> newPrice.equalsByProductCodeAndNumberAndDepartment(price))) {
                combinedPrices.add(newPrice);
            } else {
                ListIterator<Price> priceIterator = combinedPrices.listIterator();
                while (priceIterator.hasNext()) {
                    Price currentPrice = priceIterator.next();
                    if (newPrice.equalsByProductCodeAndNumberAndDepartment(currentPrice)) {
                        if (newPrice.getBegin().isBefore(currentPrice.getEnd())) {
                            if (newPrice.getValue().equals(currentPrice.getValue())) {
                                newPrice.setBegin(currentPrice.getBegin());
                                currentPrice.setEnd(newPrice.getEnd());
                            } else {
                                if (newPrice.getEnd().isBefore(currentPrice.getEnd())) {
                                    //Если мы ранее уже сохраняли новую цену
                                    if (combinedPrices.contains(newPrice)) {
                                        //И текущая цена начинается после того, как заканчивает срок действия новая добавленная цена
                                        if (currentPrice.getBegin().isAfter(newPrice.getEnd())) {
                                            //Ничего не меняем
                                            continue;
                                        } else {
                                            currentPrice.setBegin(newPrice.getEnd());
                                        }
                                    } else {
                                        Price currentPriceEnding = getPriceEnding(currentPrice, newPrice.getEnd());

                                        currentPrice.setEnd(newPrice.getBegin());
                                        priceIterator.add(newPrice);
                                        priceIterator.add(currentPriceEnding);
                                    }
                                } else if (newPrice.getBegin().isEqual(currentPrice.getBegin())) {
                                    priceIterator.remove();
                                    priceIterator.add(newPrice);
                                } else if (newPrice.getBegin().isAfter(currentPrice.getEnd())) {
                                    priceIterator.add(newPrice);
                                } else {
                                    currentPrice.setEnd(newPrice.getBegin());
                                    priceIterator.add(newPrice);
                                }
                            }
                        }
                    }
                }
            }
        }
        return combinedPrices;
    }

    /**
     * Получить остаток от текущей цены.
     * <p>
     * Остаток расчитывается в том случае, когда дата окончания новой цены меньше даты окончания текущей цены.
     * Остаток начинается с даты окончания новой цены и заканчивается датой окончания текущей.
     *
     * @param currentPrice текущая цена
     * @param newPriceEnd  дата окончания новой цены
     * @return остаток от текущей даты
     */
    private Price getPriceEnding(Price currentPrice, LocalDate newPriceEnd) {
        Price currentPriceEnding = new Price();
        currentPriceEnding.setProductCode(currentPrice.getProductCode());
        currentPriceEnding.setNumber(currentPrice.getNumber());
        currentPriceEnding.setDepartment(currentPrice.getDepartment());
        currentPriceEnding.setValue(currentPrice.getValue());
        currentPriceEnding.setBegin(newPriceEnd);
        currentPriceEnding.setEnd(currentPrice.getEnd());
        return currentPriceEnding;
    }
}
