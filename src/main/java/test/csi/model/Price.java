package test.csi.model;

import java.time.ZoneId;
import java.util.Date;
import lombok.Data;

import java.time.LocalDate;

@Data
public class Price {

    private String productCode;
    private Integer number;
    private Integer department;
    private LocalDate begin;
    private LocalDate end;
    private Long value;

    public Price() {
    }

    public Price(String productCode, Integer number, Integer department, Date begin, Date end, Integer value) {
        this.productCode = productCode;
        this.number = number;
        this.department = department;
        this.begin = begin.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();;
        this.end = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();;
        this.value = (long) value;
    }

    public boolean equalsByProductCodeAndNumberAndDepartment(Price anotherPrice) {
        if (this.productCode != null && this.productCode.equals(anotherPrice.getProductCode())
                && this.number != null && this.number.equals(anotherPrice.getNumber())
                && this.department != null && this.department.equals(anotherPrice.getDepartment())) {
            return true;
        } else {
            return false;
        }
    }

}
