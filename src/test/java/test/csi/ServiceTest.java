package test.csi;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import test.csi.model.Price;
import test.csi.service.PriceService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ServiceTest {

    @TestConfiguration
    static class ServiceTestConfiguration {

        @Bean
        public PriceService priceService() {
            return new PriceService();
        }
    }

    @Autowired
    private PriceService priceService;

    private List<Price> currentPrices = new ArrayList<>();
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    @Before
    public void setup() {
        currentPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 11_000L));
        currentPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        currentPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        currentPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 30_000L));
        currentPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 2, 25), 20_000L));
        currentPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));
    }

    private Price getPrice(String productCode, Integer number, Integer department,
                           LocalDate begin, LocalDate end, Long value) {
        Price price = new Price();
        price.setProductCode(productCode);
        price.setNumber(number);
        price.setDepartment(department);
        price.setBegin(begin);
        price.setEnd(end);
        price.setValue(value);
        return price;
    }

    @Test
    public void testCombinePrices() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 2, 20), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 15), 99_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 1, 25), 92_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 12), 5_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 12), LocalDate.of(2013, 1, 13), 4_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 13), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 2, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 20), LocalDate.of(2013, 2, 20), 11_000L));
        newPrices.add(getPrice( "122856", 2, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 1, 25), 92_000L));
        newPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 12), LocalDate.of(2013, 1, 13), 4_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testAddNewProductPrice() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 2, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));
        expectedPrices.add(getPrice("123456", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 15_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("123456", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 15_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testAddNewDepartmentPrice() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 2, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));
        expectedPrices.add(getPrice("122856", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 15_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("122856", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 15_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testAddNewNumberPrice() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 2, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));
        expectedPrices.add(getPrice("122856", 3, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 15_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("122856", 3, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 15_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testAddSameValueWithLongerDatePrice() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 3, 31), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 2, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 3, 31), 11_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testAddDifferentValueWithCrossingDatePrice() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 11_000L));
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 1, 20), 3_000L));
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 20), LocalDate.of(2013, 1, 31), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 15), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 2, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 15), LocalDate.of(2013, 1, 20), 3_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testAddDifferentValueWithCrossingTwoDatesPrice() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 10), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 3_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 20), LocalDate.of(2013, 2, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 2, 25), LocalDate.of(2013, 3, 10), 10_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 3_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testAddMultipleCrossingDates() {
        List<Price> expectedPrices = new ArrayList<>();
        expectedPrices.add(getPrice("122856", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 11_000L));
        expectedPrices.add(getPrice("122856", 2, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 99_000L));
        expectedPrices.add(getPrice("6654", 1, 2, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 31), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 1), LocalDate.of(2013, 1, 10), 30_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 3_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 20), LocalDate.of(2013, 1, 25), 20_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 25), LocalDate.of(2013, 3, 5), 5_000L));
        expectedPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 3, 5), LocalDate.of(2013, 3, 10), 10_000L));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 10), LocalDate.of(2013, 1, 20), 3_000L));
        newPrices.add(getPrice("4444", 1, 1, LocalDate.of(2013, 1, 25), LocalDate.of(2013, 3, 5), 5_000L));

        List<Price> result = priceService.combinePrices(currentPrices, newPrices);
        Assert.assertEquals(expectedPrices, result);
    }

    @Test
    public void testCombiningPrices() throws ParseException {
        List<Price> oldPrices = new ArrayList<>();
        oldPrices.add(new Price("122856", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("31.01.2013 23:59:59"), 11000));
        oldPrices.add(new Price("122856", 2, 1,
                sdf.parse("10.01.2013 00:00:00"), sdf.parse("20.01.2013 23:59:59"), 99000));
        oldPrices.add(new Price("6654", 1, 2,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("31.01.2013 00:00:00"), 5000));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(new Price("122856", 1, 1,
                sdf.parse("20.01.2013 00:00:00"), sdf.parse("22.01.2013 23:59:59"), 11000));
        newPrices.add(new Price("122856", 2, 1,
                sdf.parse("15.01.2013 00:00:00"), sdf.parse("25.01.2013 23:59:59"), 92000));
        newPrices.add(new Price("6654", 1, 2,
                sdf.parse("12.01.2013 00:00:00"), sdf.parse("13.01.2013 00:00:00"), 4000));

        List<Price> result = priceService.combinePrices(oldPrices, newPrices);

        assertEquals(6, result.size());

        assertTrue(result.contains(new Price("122856", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("22.01.2013 23:59:59"), 11000)));

        assertTrue(result.contains(new Price("122856", 2, 1,
                sdf.parse("10.01.2013 00:00:00"), sdf.parse("15.01.2013 00:00:00"), 99000)));

        assertTrue(result.contains(new Price("122856", 2, 1,
                sdf.parse("15.01.2013 00:00:00"), sdf.parse("25.01.2013 23:59:59"), 92000)));

        assertTrue(result.contains(new Price("6654", 1, 2,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("12.01.2013 00:00:00"), 5000)));

        assertTrue(result.contains(new Price("6654", 1, 2,
                sdf.parse("12.01.2013 00:00:00"), sdf.parse("13.01.2013 00:00:00"), 4000)));

        assertTrue(result.contains(new Price("6654", 1, 2,
                sdf.parse("13.01.2013 00:00:00"), sdf.parse("31.01.2013 00:00:00"), 5000)));
    }

    @Test
    public void testCombiningPricesEx() throws ParseException {
        List<Price> oldPrices = new ArrayList<>();
        oldPrices.add(new Price("122856", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("31.01.2013 23:59:59"), 11000));
        oldPrices.add(new Price("122856", 2, 1,
                sdf.parse("10.01.2013 00:00:00"), sdf.parse("20.01.2013 23:59:59"), 99000));
        oldPrices.add(new Price("6654", 1, 2,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("31.01.2013 00:00:00"), 5000));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(new Price("122856", 1, 1,
                sdf.parse("10.11.2012 00:00:00"), sdf.parse("20.01.2013 23:59:59"), 11000));
        newPrices.add(new Price("122856", 2, 1,
                sdf.parse("10.01.2013 00:00:00"), sdf.parse("25.01.2013 23:59:59"), 92000));
        newPrices.add(new Price("6654", 1, 2,
                sdf.parse("12.01.2013 00:00:00"), sdf.parse("31.01.2013 00:00:00"), 4000));

        List<Price> result = priceService.combinePrices(oldPrices, newPrices);

        assertEquals(4, result.size());
    }

    @Test
    public void testCombiningPrices2() throws ParseException {
        List<Price> oldPrices = new ArrayList<>();

        oldPrices.add(new Price("12345", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("31.01.2013 23:59:59"), 50));

        List<Price> newPrices = new ArrayList<>();

        newPrices.add(new Price("12345", 1, 1,
                sdf.parse("08.01.2013 00:00:00"), sdf.parse("14.01.2013 23:59:59"), 60));

        List<Price> result = priceService.combinePrices(oldPrices, newPrices);

        assertEquals(3, result.size());

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("08.01.2013 00:00:00"), 50)));

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("08.01.2013 00:00:00"), sdf.parse("14.01.2013 23:59:59"), 60)));

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("14.01.2013 23:59:59"), sdf.parse("31.01.2013 23:59:59"), 50)));
    }

    @Test
    public void testCombiningPrices3() throws ParseException {
        List<Price> oldPrices = new ArrayList<>();

        oldPrices.add(new Price("12345", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("15.01.2013 23:59:59"), 100));
        oldPrices.add(new Price("12345", 1, 1,
                sdf.parse("15.01.2013 00:00:00"), sdf.parse("31.01.2013 23:59:59"), 120));

        List<Price> newPrices = new ArrayList<>();

        newPrices.add(new Price("12345", 1, 1,
                sdf.parse("11.01.2013 00:00:00"), sdf.parse("17.01.2013 23:59:59"), 110));

        List<Price> result = priceService.combinePrices(oldPrices, newPrices);

        assertEquals(3, result.size());

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("11.01.2013 00:00:00"), 100)));

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("11.01.2013 00:00:00"), sdf.parse("17.01.2013 23:59:59"), 110)));

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("17.01.2013 23:59:59"), sdf.parse("31.01.2013 23:59:59"), 120)));
    }

    @Test
    public void testCombiningPrices4() throws ParseException {
        List<Price> oldPrices = new ArrayList<>();

        oldPrices.add(new Price("12345", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("10.01.2013 23:59:59"), 80));
        oldPrices.add(new Price("12345", 1, 1,
                sdf.parse("10.01.2013 00:00:00"), sdf.parse("20.01.2013 23:59:59"), 87));
        oldPrices.add(new Price("12345", 1, 1,
                sdf.parse("20.01.2013 00:00:00"), sdf.parse("31.01.2013 23:59:59"), 90));

        List<Price> newPrices = new ArrayList<>();

        newPrices.add(new Price("12345", 1, 1,
                sdf.parse("08.01.2013 00:00:00"), sdf.parse("15.01.2013 23:59:59"), 80));
        newPrices.add(new Price("12345", 1, 1,
                sdf.parse("15.01.2013 23:59:59"), sdf.parse("28.01.2013 23:59:59"), 85));

        List<Price> result = priceService.combinePrices(oldPrices, newPrices);

        assertEquals(3, result.size());

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("01.01.2013 00:00:00"), sdf.parse("15.01.2013 23:59:59"), 80)));

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("15.01.2013 23:59:59"), sdf.parse("28.01.2013 23:59:59"), 85)));

        assertTrue(result.contains(new Price("12345", 1, 1,
                sdf.parse("28.01.2013 23:59:59"), sdf.parse("31.01.2013 23:59:59"), 90)));
    }
}
